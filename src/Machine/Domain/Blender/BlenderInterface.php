<?php

declare(strict_types=1);

namespace Machine\Domain\Blender;

use Machine\Domain\Menu\Item\ItemInterface;

interface BlenderInterface
{
    public function blending(ItemInterface $blend): BlenderInterface;
}