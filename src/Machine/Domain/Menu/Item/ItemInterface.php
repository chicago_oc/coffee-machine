<?php

declare(strict_types=1);

namespace Machine\Domain\Menu\Item;

interface ItemInterface
{
    public function getName(): string;

    public function getPrice(): int;
}