<?php

declare(strict_types=1);

namespace Machine\Domain\Menu;

use Machine\Domain\Menu\Item\ItemInterface;

interface MenuInterface
{
    public function hasNumber(int $number): bool;

    public function getByNumber(int $number): ?ItemInterface;
}