<?php

declare(strict_types=1);

namespace Machine\Domain\Cashbox;

interface CashboxInterface
{
    public function pay(int $amount): CashboxInterface;

    public function giveChangeFromAmount(int $amount): bool;

    public function refund(): void;
}