<?php

declare(strict_types=1);

namespace Machine\Domain;

interface MachineInterface
{
    public function makeDeposit(int $value): MachineInterface;

    public function chooseProduct(int $number): void;

    public function cancel(): void;
}