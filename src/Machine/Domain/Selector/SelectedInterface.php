<?php

declare(strict_types=1);

namespace Machine\Domain\Selector;

/**
 * Интервейс выбора товара
 */
interface SelectedInterface
{
    public function selectProduct(int $number);
}