<?php

declare(strict_types=1);

namespace Machine\Domain\Selector;

/**
 * Интерфейс выборо товара
 */
interface SelectorInterface
{
    public function select(int $number);

    public function setOutput(SelectedInterface $output): SelectorInterface;
}