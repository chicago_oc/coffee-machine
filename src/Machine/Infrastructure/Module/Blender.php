<?php

declare(strict_types=1);

namespace Machine\Infrastructure\Module;

use Machine\Domain\Blender\BlenderInterface;
use Machine\Domain\Menu\Item\ItemInterface;

class Blender implements BlenderInterface
{
    public function blending(ItemInterface $blend): BlenderInterface
    {
        echo "blender: приготовление {$blend->getName()} ...\n";

        return $this;
    }
}