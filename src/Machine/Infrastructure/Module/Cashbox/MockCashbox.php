<?php

declare(strict_types=1);

namespace Machine\Infrastructure\Module\Cashbox;

use Machine\Domain\Cashbox\CashboxInterface;

class MockCashbox implements CashboxInterface
{
    private int $deposit;

    public function pay(int $amount): CashboxInterface
    {
        echo "cashbox: внесена сумма {$amount}\n";
        $this->deposit = $amount;
        echo "cashbox: оплата не требудется...\n";

        $this->refund();

        return $this;
    }

    public function giveChangeFromAmount(int $amount): bool
    {
        return true;
    }

    public function refund(): void
    {
        if ($this->deposit > 0) {
            echo "cashbox: возврат денежных средств {$this->deposit}...\n";
            $this->deposit = 0;
        }
    }
}