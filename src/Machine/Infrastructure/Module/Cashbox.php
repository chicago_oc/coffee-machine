<?php

declare(strict_types=1);

namespace Machine\Infrastructure\Module;

use Machine\Domain\Cashbox\CashboxInterface;

class Cashbox implements CashboxInterface
{
    private int $deposit;

    public function __construct()
    {
        $this->deposit = 0;
    }

    public function pay(int $amount): CashboxInterface
    {
        $this->deposit += $amount;
        echo 'cashbox: принято ' . $amount . ', всего ' . $this->deposit. "\n";

        return $this;
    }

    public function giveChangeFromAmount(int $amount): bool
    {
        $change = $this->deposit - $amount;
        if ($change < 0) {
            return false;
        }

        if ($change > 0) {
            echo 'cashbox: сдача ' . $change . "...\n";
        } else {
            echo "cashbox: сдача не требуется...\n";
        }

        return true;
    }

    public function refund(): void
    {
        if ($this->deposit > 0) {
            echo "cashbox: возврат денежных средств {$this->deposit}...\n";
            $this->deposit = 0;
        }
    }
}