<?php

declare(strict_types=1);

namespace Machine\Infrastructure\Module;

use Machine\Domain\Menu\Item\ItemInterface;
use Machine\Domain\Menu\MenuInterface;
use Machine\Infrastructure\Module\Menu\Item;

class Menu implements MenuInterface
{
    private array $menu;

    public function __construct(array $menu)
    {
        $this->menu = $menu;
    }

    public function hasNumber(int $number): bool
    {
        return isset($this->menu[$number]);
    }

    public function getByNumber(int $number): ?ItemInterface
    {
        if (!isset($this->menu[$number])) {
            return null;
        }

        return new Item($this->menu[$number]['name'], $this->menu[$number]['price']);
    }
}