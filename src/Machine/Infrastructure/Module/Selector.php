<?php

declare(strict_types=1);

namespace Machine\Infrastructure\Module;

use Machine\Domain\Selector\SelectedInterface;
use Machine\Domain\Selector\SelectorInterface;

class Selector implements SelectorInterface
{
    private SelectedInterface $output;

    public function setOutput(SelectedInterface $output): SelectorInterface
    {
        $this->output = $output;

        return $this;
    }

    public function select(int $number)
    {
        $this->output->selectProduct($number);
    }
}