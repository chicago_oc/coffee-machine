<?php

declare(strict_types=1);

namespace Machine\Infrastructure\Machine;

use Machine\Domain\Blender\BlenderInterface;
use Machine\Domain\Cashbox\CashboxInterface;
use Machine\Domain\MachineInterface;
use Machine\Domain\Menu\MenuInterface;
use Machine\Domain\Selector\MenuCheckerInterface;
use Machine\Domain\Selector\SelectedInterface;
use Machine\Domain\Selector\SelectorInterface;

class Machine implements MachineInterface, SelectedInterface
{
    private CashboxInterface $cashbox;
    private MenuInterface $menu;
    private BlenderInterface $blender;
    private SelectorInterface $selector;

    public function __construct(
        CashboxInterface $cashbox,
        MenuInterface $menu,
        BlenderInterface $blender,
        SelectorInterface $selector
    ) {
        $this->cashbox = $cashbox;
        $this->menu = $menu;
        $this->blender = $blender;
        $this->selector = $selector;

        $this->selector->setOutput($this);
    }

    public function makeDeposit(int $value): MachineInterface
    {
        $this->cashbox->pay($value);

        return $this;
    }

    public function chooseProduct(int $number): void
    {
        $this->selector->select($number);
    }

    public function selectProduct(int $number)
    {
        if (!$this->menu->hasNumber($number)) {
            echo "machine: неизвесный пункт меню {$number}...\n";
            return;
        }

        $product = $this->menu->getByNumber($number);
        if (!$this->cashbox->giveChangeFromAmount($product->getPrice())) {
            echo "machine: недостаточно средств, нужная сумма {$product->getPrice()}...\n";
            return;
        }

        $this->blender->blending($product);
    }

    public function cancel(): void
    {
        $this->cashbox->refund();
    }
}