<?php

declare(strict_types=1);

namespace Machine\Infrastructure\Machine;

use Machine\Domain\Blender\BlenderInterface;
use Machine\Domain\Cashbox\CashboxInterface;
use Machine\Domain\MachineInterface;
use Machine\Domain\Menu\MenuInterface;
use Machine\Domain\Selector\SelectorInterface;

/**
 * @package Machine\Infrastructure\Machine
 */
class MachineBuilder
{
    private CashboxInterface $cashbox;
    private MenuInterface $menu;
    private BlenderInterface $blender;
    private SelectorInterface $selector;

    public function setSelector(SelectorInterface $selector): MachineBuilder
    {
        $this->selector = $selector;

        return $this;
    }

    public function setCashbox(CashboxInterface $cashbox): MachineBuilder
    {
        $this->cashbox = $cashbox;

        return $this;
    }

    public function setMenu(MenuInterface $menu): MachineBuilder
    {
        $this->menu = $menu;

        return $this;
    }

    public function setBlender(BlenderInterface $blender): MachineBuilder
    {
        $this->blender = $blender;

        return $this;
    }

    public function build(): MachineInterface
    {
        return new Machine($this->cashbox, $this->menu, $this->blender, $this->selector);
    }

}