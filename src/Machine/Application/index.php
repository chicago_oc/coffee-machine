<?php

require_once '../../../vendor/autoload.php';

$builder = new \Machine\Infrastructure\Machine\MachineBuilder();
$factory = new \Machine\Application\MachineFactory($builder);
$machine = $factory->createOfficeMachine();
$machine
    ->makeDeposit(10)
    ->makeDeposit(17)
    ->chooseProduct(2);