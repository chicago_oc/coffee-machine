<?php

declare(strict_types=1);

namespace Machine\Application;

use Doctrine\ORM\Query\Expr\Select;
use Machine\Domain\MachineInterface;
use Machine\Infrastructure\Machine\MachineBuilder;
use Machine\Infrastructure\Module\Blender;
use Machine\Infrastructure\Module\Cashbox;
use Machine\Infrastructure\Module\Cashbox\MockCashbox;
use Machine\Infrastructure\Module\Menu;
use Machine\Infrastructure\Module\Selector;

class MachineFactory
{
    private MachineBuilder $builder;

    public function __construct(MachineBuilder $builder)
    {
        $this->builder = $builder;
    }

    public function createCommerceMachine(): MachineInterface
    {
        $cash = new Cashbox();
        $blender = new Blender();
        $menu = new Menu($this->getMenu());
        $selector = new Selector();

        return $this->builder
            ->setMenu($menu)
            ->setCashbox($cash)
            ->setBlender($blender)
            ->setSelector($selector)
            ->build();
    }

    public function createOfficeMachine(): MachineInterface
    {
        $menu = new Menu($this->getMenu());
        $blender = new Blender();
        $cash = new MockCashbox();
        $selector = new Selector();

        return $this->builder
            ->setCashbox($cash)
            ->setMenu($menu)
            ->setBlender($blender)
            ->setSelector($selector)
            ->build();
    }

    private function getMenu(): array
    {
        return [
            1 => [
                'name' => 'Черный кофе',
                'price' => 15,
            ],
            2 => [
                'name' => 'Капучино',
                'price' => 20,
            ],
            3 => [
                'name' => 'Латэ',
                'price' => 30,
            ],
        ];
    }
}